package com.piensaenbinario.ann.util;

import java.util.Properties;

import org.apache.log4j.Logger;

public class PropsUtil {

	static final Logger logger = Logger.getLogger(PropsUtil.class);
	
	private static Properties properties = new Properties();
	
	public PropsUtil() {
		
		try {
			properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch(Exception e){
			logger.error(e);
		}
	}
	
	/**
	 * Este metodo obtiene una propiedad como double, en caso de error devuelve el valor por defecto
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public double getDouble(String key, double defaultValue){
		
		try {
			return Double.parseDouble(properties.getProperty(key));
		} catch(Exception e) {
			return defaultValue;
		}
	}
	
	/**
	 * Este metodo obtiene una propiedad como entero, en caso de error devuelve el valor por defecto
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public int getInteger(String key, int defaultValue){
		
		try {
			return Integer.parseInt(properties.getProperty(key));
		} catch(Exception e) {
			return defaultValue;
		}
	}
	
}