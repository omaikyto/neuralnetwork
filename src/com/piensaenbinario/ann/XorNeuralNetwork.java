package com.piensaenbinario.ann;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.manhattan.ManhattanPropagation;
import org.encog.neural.networks.training.propagation.quick.QuickPropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;

import com.piensaenbinario.ann.model.Pairs;
import com.piensaenbinario.ann.util.TransformUtil;

/**
 * 
 * @author Maiky
 *
 */
public class XorNeuralNetwork {

	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("NeuralNetwork");
	private static TransformUtil transform = new TransformUtil();
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
        EntityManager em = factory.createEntityManager();
        Query q = em.createQuery("SELECT p FROM Pairs p");
        List<Pairs> pairs = q.getResultList();
        double[][]xOrInput = transform.getDoubleMatrixInputs(pairs);
        double[][] xOrIdeal = transform.getDoubleMatrixOutputs(pairs);
        
        BasicNetwork network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, 2));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, 3));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1));
        network.getStructure().finalizeStructure();
        network.reset();

        MLDataSet training = new BasicMLDataSet(xOrInput, xOrIdeal);
        
        System.out.println("#############################");
        System.out.println("1 - ManhattanPropagation");
        System.out.println("2 - ResilientPropagation");
        System.out.println("3 - QuickPropagation");
        System.out.println("Other - Backpropagation");
        System.out.print("Select training algorithm: ");
        Scanner scanner = new Scanner(System.in);
        String option = scanner.nextLine();
        scanner.close();
        
        Train train = new Backpropagation(network, training);
        
        switch (option){
			case "1":
				train = new ManhattanPropagation(network, training, 0);
				break;
			case "2":
				train = new ResilientPropagation(network, training);
				break;
			case "3":
				train = new QuickPropagation(network, training);
				break;
			default:
				break;
		}
        
        System.out.println("#Init training with algorithm: " + train.getClass().getSimpleName());
        
        int epoch = 1;
        do{
        	train.iteration();
        	System.out.println("#Epoch " + epoch + " , error: " +train.getError());
        	epoch++;
        } while( train.getError()>0.01 );
        
        System.out.println("#End training");
        
        System.out.println("#Predictions");
        
        for(MLDataPair pair:training){
        	final MLData output = network.compute(pair.getInput());
        	System.out.println(pair.getInput().getData(0) + "," + pair.getInput().getData(1) +
        		", prediction=" + output.getData(0) + ", ideal=" + pair.getIdeal().getData(0));
        }

        Encog.getInstance().shutdown();        
	}

}