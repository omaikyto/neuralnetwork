package com.piensaenbinario.ann.sevice.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import com.piensaenbinario.ann.model.Prediction;
import com.piensaenbinario.ann.sevice.PredictionService;
import com.piensaenbinario.ann.util.Constant;

public class PredictionServiceImpl implements PredictionService{

	static final Logger logger = Logger.getLogger(PredictionServiceImpl.class);

	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("NeuralNetwork");
	private static EntityManager entityManager = factory.createEntityManager();
	
	@Override
	public Prediction createPrediction(double result, double error, long time, int epoch, 
			String algorithm) {
		
		error = error/Constant.NUMBER_REP;
		result = result/Constant.NUMBER_REP;
		epoch = epoch/Constant.NUMBER_REP;
		
		if( logger.isInfoEnabled() ){
			logger.info("#Error: " + error);
			logger.info("#Result: " + result);
			logger.info("#Epoch: " + epoch);
			logger.info("#Algorithm: " + algorithm);
		}
		
		Prediction prediction = new Prediction();
        prediction.setResult(result);
        prediction.setError(error);
        prediction.setTime(time);
        prediction.setEpoch(epoch);
        prediction.setAlgorithm(algorithm);
        prediction.setDate(new Date());
        entityManager.getTransaction().begin();
        entityManager.persist(prediction);
        entityManager.getTransaction().commit();
        entityManager.close();
        return prediction;
	}
	
}