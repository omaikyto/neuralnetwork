package com.piensaenbinario.ann.sevice;

import com.piensaenbinario.ann.model.Prediction;

public interface PredictionService {

	/**
	 * Este servicio almacena una prediccion en BBDD
	 * 
	 * @param result
	 * @param error
	 * @param time
	 * @param epoch
	 * @param algorithm
	 * @return
	 */
	public Prediction createPrediction(double result, double error, long time, int epoch, 
			String algorithm);

}