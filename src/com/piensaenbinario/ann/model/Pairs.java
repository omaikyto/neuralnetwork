package com.piensaenbinario.ann.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pairs database table.
 * 
 */
@Entity
@NamedQuery(name="Pairs.findAll", query="SELECT p FROM Pairs p")
public class Pairs implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String pairId;

	private double first;

	private double out;

	private double second;

	public Pairs() {
		// Constructor por defecto ya que es una entidad de base de datos.
	}

	public String getPairId() {
		return this.pairId;
	}

	public void setPairId(String pairId) {
		this.pairId = pairId;
	}

	public double getFirst() {
		return this.first;
	}

	public void setFirst(double first) {
		this.first = first;
	}

	public double getOut() {
		return this.out;
	}

	public void setOut(double out) {
		this.out = out;
	}

	public double getSecond() {
		return this.second;
	}

	public void setSecond(double second) {
		this.second = second;
	}

}