package com.piensaenbinario.ann.util;

public class Constant {

	private Constant() {
		throw new IllegalStateException("Constant class");
	}
	
	private static PropsUtil propsUtil = new PropsUtil();
	
	public static final int NUMBER_EPOCH = propsUtil.getInteger("number-epoch", 10000);
	
	public static final int NUMBER_REP = propsUtil.getInteger("number-rep", 10);

	public static final int TIME_IN_SECONDS = propsUtil.getInteger("max-time", 10);
	
	public static final double ERROR = propsUtil.getDouble("error", 0.09);
	
}