package com.piensaenbinario.ann.util;

import java.util.List;

import org.apache.log4j.Logger;
import org.encog.ml.data.MLData;

import com.piensaenbinario.ann.model.Pairs;
import com.piensaenbinario.ann.model.Single;

public class TransformUtil {
	
	static final Logger logger = Logger.getLogger(TransformUtil.class);
	
	/**
	 * Este metodo devuelve una matriz de pares de prueba a partir de una lista de pares
	 * 
	 * @param pairs
	 * @return
	 */
	public double[][] getDoubleMatrixInputs(List<Pairs> pairs){
		
		double[][] matrix = new double[pairs.size()][2];
		
		int f = 0;
		for (Pairs pair : pairs) {
			double[] file = new double[]{pair.getFirst(),pair.getSecond()};
			matrix[f] = file;
			f++;
		}
		
		return matrix;
	}
	
	/**
	 * Este metodo devuelve una matriz de salidas esperadas a partir de una lista de pares
	 * 
	 * @param pairs
	 * @return
	 */
	public double[][] getDoubleMatrixOutputs(List<Pairs> pairs){
		
		double[][] matrix = new double[pairs.size()][1];
		
		int f = 0;
		for (Pairs pair : pairs) {
			double[] file = new double[]{pair.getOut()};
			matrix[f] = file;
			f++;
		}
		
		return matrix;
	}
	
	/**
	 * Este metodo devuelve una matriz de entradas de prueba a partir de una lista de entradas
	 * 
	 * @param pairs
	 * @param numberOfBits
	 * @return
	 */
	public double[][] getDoubleMatrixSingleInputs(List<Single> singles, int numberOfBits){
		
		double[][] matrix = new double[singles.size()][numberOfBits];
		
		int j = 0;
		for (Single single : singles) {
			int decimal = single.getInput();
			String binary = Integer.toBinaryString(decimal);
			String[] binary2 = binary.split("");
			double[] file = new double[numberOfBits];
			int i = numberOfBits-binary2.length;
			for (String b : binary2) {
				try{
					file[i] = Double.valueOf(b);
					i++;
				} catch (NumberFormatException e){
					logger.error(e);
				}
			}
			matrix[j] = file;
			j++;
		}
		
		return matrix;
	}
	
	/**
	 * Este metodo devuelve una matriz de salidas esperadas a partir de una lista de entradas
	 * 
	 * @param pairs
	 * @param numberOfBits
	 * @return
	 */
	public double[][] getDoubleMatrixSingleOutputs(List<Single> singles, int numberOfBits){
		
		double[][] matrix = new double[singles.size()][numberOfBits];
		
		int j = 0;
		for (Single single : singles) {
			int decimal = single.getOutput();
			String binary = Integer.toBinaryString(decimal);
			String[] binary2 = binary.split("");
			double[] file = new double[numberOfBits];
			int i = numberOfBits-binary2.length;
			for (String b : binary2) {
				try{
					file[i] = Double.valueOf(b);
					i++;
				} catch (NumberFormatException e){
					logger.error(e);
				}
			}
			matrix[j] = file;
			j++;
		}
		
		return matrix;
	}
	
	/**
	 * Este metodo pasa de binario a decimal
	 * 
	 * @param output
	 * @return
	 */
	public double normalize(MLData output){
		
		double result = 0.0;
		int exp = 0;
		for (int i=output.size()-1;i>=0;i--){
			result += Math.round(output.getData(i)) * Math.pow(2,exp);
			exp++;
		}
		
		return result;
	}
	
}