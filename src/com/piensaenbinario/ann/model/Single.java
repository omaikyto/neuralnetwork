package com.piensaenbinario.ann.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the single database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Single.findAll", query="SELECT s FROM Single s"),
})
public class Single implements Serializable {
	private static final long serialVersionUID = 1L;
	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int singleId;
	
	private int input;
	
	private int output;
	
	private Date date;

	public Single() {
		// Constructor por defecto ya que es una entidad de base de datos.
	}   
	
	public int getSingleId() {
		return this.singleId;
	}

	public void setSingleId(int singleId) {
		this.singleId = singleId;
	}   
	public int getInput() {
		return this.input;
	}

	public void setInput(int input) {
		this.input = input;
	}   
	public int getOutput() {
		return this.output;
	}

	public void setOutput(int output) {
		this.output = output;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
   
}
