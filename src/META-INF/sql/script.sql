-- ----------------------------
-- Database
-- ----------------------------
CREATE DATABASE `piensaenbinario_rna`;

-- ----------------------------
-- Go to database
-- ----------------------------
USE piensaenbinario_rna;

-- ----------------------------
-- Table structure for pairs
-- ----------------------------
DROP TABLE IF EXISTS `pairs`;
CREATE TABLE `pairs` (
  `pairId` int(11) NOT NULL AUTO_INCREMENT,
  `first` double(11,0) DEFAULT NULL,
  `second` double(11,0) DEFAULT NULL,
  `out` double(11,0) DEFAULT NULL,
  PRIMARY KEY (`pairId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pairs
-- ----------------------------
INSERT INTO `pairs` VALUES ('1', '0', '0', '0');
INSERT INTO `pairs` VALUES ('2', '1', '0', '1');
INSERT INTO `pairs` VALUES ('3', '0', '1', '1');
INSERT INTO `pairs` VALUES ('4', '1', '1', '0');

-- ----------------------------
-- Table structure for pairs
-- ----------------------------
DROP TABLE IF EXISTS `single`;
CREATE TABLE `single` (
  `singleId` int(11) NOT NULL AUTO_INCREMENT,
  `input` int(11) DEFAULT NULL,
  `output` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`singleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of single
-- ----------------------------
INSERT INTO `single` VALUES ('1', '0', '5', '2016/03/01');
INSERT INTO `single` VALUES ('2', '5', '14', '2016/04/01');
INSERT INTO `single` VALUES ('3', '14', '12', '2016/05/01');
INSERT INTO `single` VALUES ('4', '12', '15', '2016/06/01');
INSERT INTO `single` VALUES ('5', '15', '16', '2016/07/01');
INSERT INTO `single` VALUES ('6', '16', '8', '2016/08/01');
INSERT INTO `single` VALUES ('7', '8', '15', '2016/09/01');
INSERT INTO `single` VALUES ('8', '15', '13', '2016/10/01');
INSERT INTO `single` VALUES ('9', '13', '39', '2016/11/01');
INSERT INTO `single` VALUES ('10', '39', '48', '2016/12/01');
INSERT INTO `single` VALUES ('11', '48', '57', '2017/01/01');
INSERT INTO `single` VALUES ('12', '57', '56', '2017/02/01');
INSERT INTO `single` VALUES ('13', '56', '105', '2017/03/01');
INSERT INTO `single` VALUES ('14', '105', '84', '2017/04/01');
INSERT INTO `single` VALUES ('15', '84', '117', '2017/05/01');
INSERT INTO `single` VALUES ('16', '117', '111', '2017/06/01');
INSERT INTO `single` VALUES ('17', '111', '106', '2017/07/01');
INSERT INTO `single` VALUES ('18', '106', '101', '2017/08/01');
INSERT INTO `single` VALUES ('19', '101', '99', '2017/09/01');
INSERT INTO `single` VALUES ('20', '99', '125', '2017/10/01');
INSERT INTO `single` VALUES ('21', '125', '147', '2017/11/01');
INSERT INTO `single` VALUES ('22', '147', '116', '2017/12/01');
INSERT INTO `single` VALUES ('23', '116', '142', '2018/01/01');
INSERT INTO `single` VALUES ('24', '142', '189', '2018/02/01');
INSERT INTO `single` VALUES ('25', '189', '343', '2018/03/01');
INSERT INTO `single` VALUES ('26', '343', '278', '2018/04/01');
INSERT INTO `single` VALUES ('27', '278', '377', '2018/05/01');
INSERT INTO `single` VALUES ('28', '377', '371', '2018/06/01');
INSERT INTO `single` VALUES ('29', '371', '303', '2018/07/01');
INSERT INTO `single` VALUES ('30', '303', '297', '2018/08/01');
INSERT INTO `single` VALUES ('31', '297', '274', '2018/09/01');
INSERT INTO `single` VALUES ('32', '274', '457', '2018/10/01');

-- ----------------------------
-- Table structure for prediction
-- ----------------------------
DROP TABLE IF EXISTS `prediction`;
CREATE TABLE `prediction` (
  `predictionId` int(11) NOT NULL AUTO_INCREMENT,
  `result` double(11,0) DEFAULT NULL,
  `error` double(11,10) DEFAULT NULL,
  `time` bigint NULL DEFAULT NULL,
  `epoch` int(11) DEFAULT NULL,
  `algorithm` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`predictionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `userId` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `logger` varchar(250) NOT NULL,
  `level` varchar(10) NOT NULL,
  `message` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Create user
-- ----------------------------
CREATE USER 'ann'@'localhost' IDENTIFIED BY 'piensaenbinario_ann';
GRANT ALL PRIVILEGES ON piensaenbinario_rna.* TO 'ann'@'localhost';
FLUSH PRIVILEGES;