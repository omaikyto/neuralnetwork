package com.piensaenbinario.ann.util;

import java.util.List;

import com.piensaenbinario.ann.model.Single;

public class CalcUtil {

	/**
	 * Este metodo devuelve el valor maximo de todos los casos de prueba
	 * 
	 * @param singles
	 * @return
	 */
	public int getMaxValue(List<Single> singles) {
		
		int maxValue = 0;
		for (Single single : singles) {
			if( maxValue<single.getInput() ){
				maxValue = single.getInput();
			}
			if( maxValue<single.getOutput() ){
				maxValue = single.getOutput();
			}
		}
		
		return maxValue;
	}
	
	/**
	 * Este metodo devuelve el numero de bits necesarios para denotar en binario un numero decimal
	 * 
	 * @param number
	 * @return
	 */
	public int getNumberOfBitsRequired(int number) {
		
		int exp = 0;
		do {
			exp++;
		}while( Math.pow(2,exp)<number );
		
		return exp + 1;
	}
	
}