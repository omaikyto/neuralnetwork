package com.piensaenbinario.ann.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the prediction database table.
 * 
 */
@Entity
@NamedQuery(name="Prediction.findAll", query="SELECT p FROM Prediction p")
public class Prediction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int predictionId;

	private String algorithm;

	private long epoch;

	private double error;

	private double result;

	private long time;
	
	private Date date;

	public Prediction() {
		// Constructor por defecto ya que es una entidad de base de datos.
	}

	public int getPredictionId() {
		return this.predictionId;
	}

	public void setPredictionId(int predictionId) {
		this.predictionId = predictionId;
	}

	public String getAlgorithm() {
		return this.algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public long getEpoch() {
		return this.epoch;
	}

	public void setEpoch(long epoch) {
		this.epoch = epoch;
	}

	public double getError() {
		return this.error;
	}

	public void setError(double error) {
		this.error = error;
	}

	public double getResult() {
		return this.result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public long getTime() {
		return this.time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}