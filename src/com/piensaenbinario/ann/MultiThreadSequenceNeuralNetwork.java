package com.piensaenbinario.ann;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationElliott;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

import com.piensaenbinario.ann.model.Single;
import com.piensaenbinario.ann.sevice.impl.PredictionServiceImpl;
import com.piensaenbinario.ann.thread.TrainThread;
import com.piensaenbinario.ann.util.CalcUtil;
import com.piensaenbinario.ann.util.Constant;
import com.piensaenbinario.ann.util.TransformUtil;

/**
 * 
 * @author Maiky
 *
 */
public class MultiThreadSequenceNeuralNetwork {

	static final Logger logger = Logger.getLogger(MultiThreadSequenceNeuralNetwork.class);
	
	private static CalcUtil calc = new CalcUtil();
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("NeuralNetwork");
	private static TransformUtil transform = new TransformUtil();
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		EntityManager entityManager = factory.createEntityManager();
        Query q = entityManager.createNamedQuery("Single.findAll");
        List<Single> singles = q.getResultList();
        
        int maxValue = calc.getMaxValue(singles);
        int numberOfBits = calc.getNumberOfBitsRequired(maxValue);
        
        double[][] inputs = transform.getDoubleMatrixSingleInputs(singles, numberOfBits);
        double[][] outputs = transform.getDoubleMatrixSingleOutputs(singles, numberOfBits);
        
        BasicNetwork network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), false, numberOfBits));
        network.getStructure().finalizeStructure();
        network.reset();
        
        if( logger.isDebugEnabled() ){
        	logger.debug("#Number of repetitions: " + Constant.NUMBER_REP);
        	logger.debug("#Number of cores: " + Runtime.getRuntime().availableProcessors());
        }
        
        long initTime = System.currentTimeMillis();
        
        TrainThread[] trainThreads = new TrainThread[Constant.NUMBER_REP];
        for (int i = 0; i < Constant.NUMBER_REP; i++) {
        	trainThreads[i] = new TrainThread(network, inputs, outputs, maxValue, numberOfBits);
        	trainThreads[i].start();
		}
        for (TrainThread trainThread : trainThreads) {
        	try {
        		trainThread.join();
        	} catch (InterruptedException e) {
        		logger.error(e);
        		Thread.currentThread().interrupt();
        	}
		}
        
        String algorithm = TrainThread.getAlgorithm();
        double error = TrainThread.getError();
        double result = TrainThread.getResult();
        int totalEpoch = TrainThread.getTotalEpoch();
        
        PredictionServiceImpl predictionService = new PredictionServiceImpl();
        predictionService.createPrediction(result, error, System.currentTimeMillis()-initTime, totalEpoch, algorithm);
        
        Encog.getInstance().shutdown();
	}
	
}
