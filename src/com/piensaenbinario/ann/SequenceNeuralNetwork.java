package com.piensaenbinario.ann;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationElliott;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.back.Backpropagation;

import com.piensaenbinario.ann.model.Single;
import com.piensaenbinario.ann.sevice.impl.PredictionServiceImpl;
import com.piensaenbinario.ann.util.CalcUtil;
import com.piensaenbinario.ann.util.Constant;
import com.piensaenbinario.ann.util.TransformUtil;

/**
 * 
 * @author Maiky
 *
 */
public class SequenceNeuralNetwork {

	static final Logger logger = Logger.getLogger(SequenceNeuralNetwork.class);
	private static CalcUtil calc = new CalcUtil();
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("NeuralNetwork");
	private static TransformUtil transform = new TransformUtil();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		EntityManager entityManager = factory.createEntityManager();
        Query q = entityManager.createNamedQuery("Single.findAll");
        List<Single> singles = q.getResultList();
        
        int maxValue = calc.getMaxValue(singles);
        int numberOfBits = calc.getNumberOfBitsRequired(maxValue);
        double error = 0.0;
        double result = 0.0;
        int totalEpoch = 0;
        String algorithm = "";
        
        double[][] inputs = transform.getDoubleMatrixSingleInputs(singles, numberOfBits);
        double[][] outputs = transform.getDoubleMatrixSingleOutputs(singles, numberOfBits);
        
        BasicNetwork network = new BasicNetwork();
        network.addLayer(new BasicLayer(null, true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits));
        network.addLayer(new BasicLayer(new ActivationElliott(), true, numberOfBits-1));
        network.addLayer(new BasicLayer(new ActivationElliott(), false, numberOfBits));
        network.getStructure().finalizeStructure();
        network.reset();
        
        
        if( logger.isDebugEnabled() ){
        	logger.debug("#Number of repetitions: " + Constant.NUMBER_REP);
        	logger.debug("#Number of cores: " + Runtime.getRuntime().availableProcessors());
        }
        
        long initTime = System.currentTimeMillis();
        
        for (int i = 1; i <= Constant.NUMBER_REP; i++) {
        	if( logger.isDebugEnabled() ){
        		logger.debug("#Init training " + i);
        	}
        	long time = System.currentTimeMillis();
        	
        	MLDataSet training = new BasicMLDataSet(inputs, outputs);
        	Train train = new Backpropagation(network, training);
        	algorithm = train.getClass().getSimpleName();
        	
        	if( logger.isDebugEnabled() ){
        		logger.debug("#Max value: " + maxValue);
        		logger.debug("#Number of bits: " + numberOfBits);
        		logger.debug("#Max time in seconds: " + Constant.TIME_IN_SECONDS);
        		logger.debug("#Init training with algorithm: " + train.getClass().getSimpleName());
        	}
        	
        	int epoch = 1;
        	boolean sigue = true;
        	do{
        		train.iteration();
        		epoch++;
        		sigue = train.getError()>Constant.ERROR && System.currentTimeMillis()-time<Constant.TIME_IN_SECONDS*1000;
        		if( epoch%Constant.NUMBER_EPOCH==0 ){
        			logger.info("{\"epoch\": " + epoch + ", \"error\": " + train.getError() + ", \"time\": " 
        					+ (System.currentTimeMillis()-time)/1000 + "}");
        		}
        	} while( sigue );
        	
        	if( logger.isDebugEnabled() ){
        		logger.debug("#End training " + i + " in " + epoch + " epochs with error " + train.getError() + " in " 
        				+ (System.currentTimeMillis()-time)/1000 + "s");
        	}
        	
        	MLData input = new BasicMLData(1);
        	input.setData(outputs[outputs.length-1]);
        	final MLData output = network.compute(input);
        	
        	result += transform.normalize(output);
        	error += train.getError();
        	totalEpoch +=epoch;
        	network.reset();
		}
        
        PredictionServiceImpl predictionService = new PredictionServiceImpl();
        predictionService.createPrediction(result, error, System.currentTimeMillis()-initTime, totalEpoch, algorithm);
        
        Encog.getInstance().shutdown();
	}

}