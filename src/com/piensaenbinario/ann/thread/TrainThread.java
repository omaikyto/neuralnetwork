package com.piensaenbinario.ann.thread;

import org.apache.log4j.Logger;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.back.Backpropagation;

import com.piensaenbinario.ann.util.Constant;
import com.piensaenbinario.ann.util.TransformUtil;

public class TrainThread extends Thread{

	static final Logger logger = Logger.getLogger(TrainThread.class);
	
	private static Double error = 0.0;
	private static Double result = 0.0;
	private static Integer totalEpoch = 0;
	private static String algorithm = "";
	private static TransformUtil transform = new TransformUtil();
	
	private BasicNetwork network;
	private int maxValue;
	private int numberOfBits;
	private double[][] inputs;
	private double[][] outputs;

	/**
	 * Constructor
	 * 
	 * @param network
	 * @param inputs
	 * @param outputs
	 * @param maxValue
	 * @param numberOfBits
	 */
	public TrainThread(BasicNetwork network, double[][] inputs, double[][] outputs, int maxValue, int numberOfBits) {
		this.setNetwork(network);
		this.setMaxValue(maxValue);
		this.setNumberOfBits(numberOfBits);
		this.setInputs(inputs);
		this.setOutputs(outputs);
	}
	
	@Override
	public void run(){
		
		if( logger.isDebugEnabled() ){
    		logger.debug("#Init training " + this.getName());
    	}
		
    	long time = System.currentTimeMillis();
    	
    	MLDataSet training = new BasicMLDataSet(getInputs(), getOutputs());
        Train train = new Backpropagation(network, training);
 
        setAlgorithm(train.getClass().getSimpleName());
    	
    	if( logger.isDebugEnabled() ){
    		logger.debug("#Max value: " + maxValue);
    		logger.debug("#Number of bits: " + numberOfBits);
    		logger.debug("#Max time in seconds: " + Constant.TIME_IN_SECONDS);
    		logger.debug("#Init training with algorithm: " + algorithm);
    	}
    	
    	int epoch = 1;
    	boolean sigue = true;
    	do{
    		train.iteration();
    		epoch++;
    		sigue = train.getError()>Constant.ERROR && System.currentTimeMillis()-time<Constant.TIME_IN_SECONDS*1000;
    		if( epoch%Constant.NUMBER_EPOCH==0 ){
    			logger.info("{\"epoch\": " + epoch + ", \"error\": " + train.getError() + ", \"time\": " 
    					+ (System.currentTimeMillis()-time)/1000 + "}");
    		}
    	} while( sigue );
    	
    	if( logger.isDebugEnabled() ){
    		logger.debug("#End training " + this.getName() + " in " + epoch + " epochs with error " + train.getError() 
    				+ " in " + (System.currentTimeMillis()-time)/1000 + "s");
    	}
    	
    	MLData input = new BasicMLData(1);
    	input.setData(getOutputs()[getOutputs().length-1]);
    	final MLData output = network.compute(input);
    	
    	setError(getError()+train.getError());
    	setResult(getResult()+transform.normalize(output));
    	setTotalEpoch(getTotalEpoch()+epoch);
    	network.reset();
    }

	/**
	 * Metodos getter y setter
	 */
	public BasicNetwork getNetwork() {
		return network;
	}

	public void setNetwork(BasicNetwork network) {
		this.network = network;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public int getNumberOfBits() {
		return numberOfBits;
	}

	public void setNumberOfBits(int numberOfBits) {
		this.numberOfBits = numberOfBits;
	}

	public double[][] getInputs() {
		return inputs;
	}
	
	public void setInputs(double[][] inputs) {
		this.inputs = inputs;
	}

	public double[][] getOutputs() {
		return outputs;
	}

	public void setOutputs(double[][] outputs) {
		this.outputs = outputs;
	}

	public static Integer getTotalEpoch() {
		return totalEpoch;
	}
	
	public static void setTotalEpoch(Integer totalEpoch) {
		TrainThread.totalEpoch = totalEpoch;
	}

	public static Double getError() {
		return error;
	}
	
	public static void setError(Double error) {
		TrainThread.error = error;
		
	}
	
	public static Double getResult() {
		return result;
	}
	
	public static void setResult(Double result) {
		TrainThread.result = result;
		
	}

	public static String getAlgorithm() {
		return algorithm;
	}
	
	public static void setAlgorithm(String algorithm) {
		TrainThread.algorithm = algorithm;
	}
	
}